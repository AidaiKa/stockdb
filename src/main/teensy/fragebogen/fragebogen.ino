#include<ADC.h>
#include<EEPROM.h>

/////////////////////////////////////////////////////
// Global constants
/////////////////////////////////////////////////////
#define FIRMWARE_ID         0xAC
#define PWM_PIN             3

/////////////////////////////////////////////////////
// List of a commands
/////////////////////////////////////////////////////
#define CMD_GET_ID          0
#define CMD_GET_ADC         1
#define CMD_SET_LIMITS      2
#define CMD_SET_PWM         3
#define CMD_PAUSE           4
#define CMD_BLINK_ON        5
#define CMD_BLINK_OFF       6
#define CMD_FIXOUT          7
#define CMD_FIXRESET        8

/////////////////////////////////////////////////////
// EEPROM offsets
/////////////////////////////////////////////////////
#define EEPROM_LIMIT_OFFS   0x00    // low_limit value offset in EEPROM
#define EEPROM_SIZE_OFFS    0x02    // area_size value offset in EEPROM
#define EEPROM_FREQ_OFFS    0x04    // pwm_freq value offset in EEPROM
#define BLINK_TICK_LIMIT    500

/////////////////////////////////////////////////////
// Global variables
/////////////////////////////////////////////////////
ADC *adc = new ADC();                     // ADC object

uint8_t NO_OUT        =     1;
uint8_t FIX_OUT       =     0;
uint8_t BLINK_ON      =     0;
uint8_t blink_current =     0;            // Current blink state
uint16_t blink_tick   =     0;

uint16_t low_limit    =     0x0000;       // PWM high/low limits
uint16_t area_size    =     0xFFFF;
uint16_t pwm_freq     =     100;

/////////////////////////////////////////////////////
// Initialization
/////////////////////////////////////////////////////
void setup() {
  Serial.begin(115200);
  
  initADC();

  // Blink with a LED
  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);
  delay(1000);
  digitalWrite(13, LOW);

  // Read EEPROM data
  low_limit = EEPROM.read(EEPROM_LIMIT_OFFS);
  low_limit += EEPROM.read(EEPROM_LIMIT_OFFS + 1) << 8;

  area_size = EEPROM.read(EEPROM_SIZE_OFFS);
  area_size += EEPROM.read(EEPROM_SIZE_OFFS + 1) << 8;

  pwm_freq = EEPROM.read(EEPROM_FREQ_OFFS);
  pwm_freq += EEPROM.read(EEPROM_FREQ_OFFS + 1) << 8;

  // Setup PWM
  analogWriteFrequency(PWM_PIN, pwm_freq);
  analogWriteResolution(16);
}

/////////////////////////////////////////////////////
// Save the current settings to the EEPROM
/////////////////////////////////////////////////////
void saveEEPROM() {
  EEPROM.write(EEPROM_LIMIT_OFFS, low_limit & 0xFF);
  EEPROM.write(EEPROM_LIMIT_OFFS + 1, (low_limit >> 8) & 0xFF);

  EEPROM.write(EEPROM_SIZE_OFFS, area_size & 0xFF);
  EEPROM.write(EEPROM_SIZE_OFFS + 1, (area_size >> 8) & 0xFF);

  EEPROM.write(EEPROM_FREQ_OFFS, pwm_freq & 0xFF);
  EEPROM.write(EEPROM_FREQ_OFFS + 1, (pwm_freq >> 8) & 0xFF);
}

/////////////////////////////////////////////////////
// Initialize ADC
/////////////////////////////////////////////////////
void initADC() {
    pinMode(A0, INPUT);

    ///// ADC0 ////
    adc->setReference(ADC_REFERENCE::REF_3V3, ADC_0);

    adc->setAveraging(200);     // set number of averages
    adc->setResolution(16);     // set bits of resolution

    adc->setConversionSpeed(ADC_CONVERSION_SPEED::MED_SPEED);         // VERY_LOW_SPEED, LOW_SPEED, MED_SPEED, HIGH_SPEED_16BITS, HIGH_SPEED or VERY_HIGH_SPEED
    adc->setSamplingSpeed(ADC_SAMPLING_SPEED::MED_SPEED);             // VERY_LOW_SPEED, LOW_SPEED, MED_SPEED, HIGH_SPEED or VERY_HIGH_SPEED

    //adc->startContinuous(A0, ADC_0);

    delay(500);
}

uint16_t    adc_data;
uint8_t     out_low;
uint16_t    pwm_out;

/////////////////////////////////////////////////////
// Main loop
/////////////////////////////////////////////////////
void loop() {
  adc_data = adc->analogRead(A0);

  if (!FIX_OUT) {
    pwm_out = low_limit + area_size * ((double)adc_data / (double)0xFFFF);
  }
  
  //analogWrite(PWM_PIN, pwm_out);
  if (FIX_OUT & !NO_OUT) {
    analogWrite(PWM_PIN, 0xFFFF - pwm_out);
  } else if (NO_OUT) {
    analogWrite(PWM_PIN, 0xFD70); 
  } else {
    if (!BLINK_ON) {
      analogWrite(PWM_PIN, 0xFFFF - pwm_out);
    } else {
      blink_tick++;
      if (blink_tick > BLINK_TICK_LIMIT) {
        blink_tick = 0;
        out_low = !out_low;
      }

      if (out_low) {
        analogWrite(PWM_PIN, 0xFD70);
      } else if (blink_tick == 1) {
        // Soft start
        uint16_t start_steps = 3 * log10(pwm_out);
        uint16_t sleep_time = 2000 / pwm_freq;        // (1 / f) * 1000 * 2, 1/f = T, 1000 ms/s
      //  int sleep_time = 1000 / pwm_freq;        // (1 / f) * 1000 * 2, 1/f = T, 1000 ms/s
        uint16_t pwm_inc = pwm_out / start_steps;
        for (int i = 1; i <= start_steps; i++) {
          analogWrite(PWM_PIN, 0xFFFF - pwm_inc * i);
          delay(sleep_time);
        }
      } else {
        analogWrite(PWM_PIN, 0xFFFF - pwm_out);
      }
    }
  }

  if (Serial.available()) {
    uint8_t cmd = Serial.read();
    
    switch (cmd) {
      case CMD_GET_ID:
        Serial.write(FIRMWARE_ID);
        break;
        
      case CMD_GET_ADC:
        Serial.write(adc_data & 0xFF);
        Serial.write((adc_data >> 8) & 0xFF);
        NO_OUT = 0;
        break;
        
      case CMD_SET_LIMITS:
        low_limit = Serial.read();
        low_limit += (Serial.read() << 8);

        area_size = Serial.read();
        area_size += (Serial.read() << 8);

        saveEEPROM();
        NO_OUT = 0;
        break;
        
      case CMD_SET_PWM:
        pwm_freq = Serial.read();
        pwm_freq += (Serial.read() << 8);
        analogWriteFrequency(PWM_PIN, pwm_freq);

        saveEEPROM();
        NO_OUT = 0;
        break;

      case CMD_PAUSE:
        NO_OUT = 1;
        break;

      case CMD_BLINK_ON:
        BLINK_ON = 1;
        break;

      case CMD_BLINK_OFF:
        BLINK_ON = 0;
        break;

      case CMD_FIXOUT:
        FIX_OUT = 1;
        NO_OUT = 1;
        break;

      case CMD_FIXRESET:
        FIX_OUT = 0;
        break;
    } // switch (cmd)
  } // if Serial.available()
}

