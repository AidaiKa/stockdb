package de.malex.stockdb.db;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import de.malex.stockdb.db.model.Stock;

/**
 * Facade for access to database
 * 
 * @author Alexandr Mitiaev
 */
public class DBFacade {
	
	/**
	 * The current {@link Session}
	 */
	public static Session session = null;
	
	/**
	 * Open new database session
	 */
	public static void openSession() {
		try {
			Database db = Database.getInstance();
		
			if (session != null) {
				db.closeSession();
			}
		
			db.openSession();
			session = db.getSession();
		} catch (Exception e) {
		}
	}
	
	public static void reOpenSession(final String dbFile) {
		try {
			Database db = Database.getInstance();
			db.reConfigureDB(dbFile);
			session = db.getSession();
		} catch (Exception e) {
		}
	}
	
	/**
	 * Close current database session
	 */
	public static void closeSession() {
		try {
			Database db = Database.getInstance();
			
			if (session != null) {
				db.closeSession();
				session = null;
			}
		} catch (DatabaseException e) {
		}
	}
	
	/**
	 * Remove the {@link Stock} entry from the database
	 * 
	 * @param user the {@link Stock} to remove
	 * 
	 * @throws DatabaseException 
	 */
	public static void removeUser(final Stock user) throws DatabaseException {
		if (session == null)
			openSession();
		
		Transaction transaction = session.beginTransaction();
		
		try {
			session.delete(user);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			throw new DatabaseException("Failed to remove user data");
		}
	}
	
	/**
	 * Return list of {@link Stock}s from the database
	 * 
	 * @return A list of the {@link Stock} entitys
	 */
	@SuppressWarnings("unchecked")
	public static List<Stock> getAllStocks() {

		if (session == null)
			openSession();

		return session.createCriteria(Stock.class).list();
	}
	
	/**
	 * Get user by ID
	 * 
	 * @param id	The ID of the user
	 * 
	 * @return		The {@link Stock}
	 */
	public static Stock getUser(final long id) {
		return (Stock) session.get(Stock.class, id);
	}
	
	/**
	 * Fill the {@link Experiment} object with a input/output data
	 * 
	 * @param experiment the {@link Experiment} to fill
	 */
	/*
	public static void getExperimentData(Experiment experiment) {
		if (experiment.getSpsValues().size() > 0)
			experiment.ioInitialize();
		else
			Hibernate.initialize(experiment.getSpsValues());
	}
	*/

	/**
	 * Save the {@link Stock} into database in one transaction
	 * 
	 * @param user 		The {@link Stock} instance to save
	 * 
	 * @throws DatabaseException 
	 */
	public static void saveUser(final Stock user) throws DatabaseException {
		
		if (session == null)
			openSession();
		
		Transaction transaction = session.beginTransaction();
			
		try {
			session.save(user);
			session.flush();
				
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			throw new DatabaseException("Failed to save user data");
		}
	}
}
