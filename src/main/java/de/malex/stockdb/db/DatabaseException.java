package de.malex.stockdb.db;

/**
 * Exception class for {@link DBFacade}
 * 
 * @author Alexandr Mitiaev
 */
public class DatabaseException extends Exception
{
	/**
	 * Generated serial version UID 
	 */
	private static final long serialVersionUID = 5200850258324401730L;

	/**
	 * Create new {@link DatabaseException} object
	 */
	public DatabaseException() {
	}

	/**
	 * Create new {@link DatabaseException} object with error code = 0
	 * 
	 * @param message Error message
	 */
	public DatabaseException(String message) {
		super(message);
	}
}
