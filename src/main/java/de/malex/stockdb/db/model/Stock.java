package de.malex.stockdb.db.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Entity class "Stock"
 * 
 * @author Alexandr Mitiaev
 * 
 */
@Entity
@Table(name = "stocks")
public class Stock
{
	/**
	 * Stock ID
	 */
	@Id
	@Column(name="id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * Stock name
	 */
	@Column(name = "name", length=256)
	private String name;
	
	/**
	 * Stock symbol
	 */
	@Column(name = "symbol", length=16)
	private String symbol;
	
	/**
	 * List of buyed stocks
	 */
	@OneToMany(mappedBy = "stock",
			   cascade = CascadeType.ALL,
			   orphanRemoval = true)
	private List<StockBuy> buyList = new ArrayList<>();
	
	
	/**
	 * List of selled stocks
	 */
	@OneToMany(mappedBy = "stock",
			   cascade = CascadeType.ALL,
			   orphanRemoval = true)
	private List<StockSell> sellList = new ArrayList<>();

	/**
	 * Create new {@link Stock} object
	 */
	public Stock() {
	}

	/**
	 * Returns the {@link #id}
	 * 
	 * @return	The {@link #id} value
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets new value of the {@link #id} field
	 * 
	 * @param userId	The new value to set
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * Returns the {@link #name}
	 * 
	 * @return	The {@link #name} value
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets new value of the {@link #name} field
	 * 
	 * @param sex	The new value to set
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Returns the {@link #symbol}
	 * 
	 * @return	The {@link #symbol} value
	 */
	public String getSymbol() {
		return symbol;
	}

	/**
	 * Sets new value of the {@link #symbol} field
	 * 
	 * @param age	The new value to set
	 */
	public void setSymbol(final String symbol) {
		this.symbol = symbol;
	}
	
	/**
	 * Returns the buy orders for this stock
	 * 
	 * @return	The array list of {@link StockBuy}
	 */
	public List<StockBuy> getBuyOrders() {
		return buyList;
	}
	
	/**
	 * Sets the buy orders for this stock
	 * 
	 * @param orders	The order list to set
	 */
	public void setBuyOrders(final List<StockBuy> orders) {
		this.buyList = orders;
	}
	
	/**
	 * Append one buy order to the order list
	 * 
	 * @param order	The {@link StockBuy} order to append
	 */
	public void addBuyOrder(final StockBuy order) {
        buyList.add(order);
        order.setStock(this);
    }
 
	/**
	 * Remove one buy order from the order list
	 * 
	 * @param order The {@link StockBuy} order to remove
	 */
    public void removeBuyOrder(final StockBuy order) {
        buyList.remove(order);
        order.setStock(null);
    }
    
    /**
	 * Returns the sell orders for this stock
	 * 
	 * @return	The array list of {@link StockSell}
	 */
	public List<StockSell> getSellOrders() {
		return sellList;
	}
	
	/**
	 * Sets the sell orders for this stock
	 * 
	 * @param orders	The order list to set
	 */
	public void setSellOrders(final List<StockSell> orders) {
		this.sellList = orders;
	}
	
	/**
	 * Append one sell order to the order list
	 * 
	 * @param order	The {@link StockSell} order to append
	 */
	public void addSellOrder(final StockSell order) {
		if (sellList.indexOf(order) == -1) {
			sellList.add(order);
		}
		
        order.setStock(this);
    }
 
	/**
	 * Remove one sell order from the order list
	 * 
	 * @param order The {@link StockSell} order to remove
	 */
    public void removeSellOrder(final StockSell order) {
    	if (!(sellList.indexOf(order) == -1)) {
    		sellList.remove(order);
    	}
    	
        order.setStock(null);
    }
    
    /**
     * Returns number of selled stocks
     * 
     * @return
     */
    public int getSellCount() {
    	if (sellList != null) {
    		int count = 0;
    		
    		for (final StockSell sellOrder: sellList) {
    			count += sellOrder.getAmount();
    		}
    		
    		return count;
    	} else {
    		return 0;
    	}
    }
    
    /**
     * Returns number of buyed stocks
     * 
     * @return
     */
    public int getBuyCount() {
    	if (buyList != null) {
    		int count = 0;
    		
    		for (final StockBuy buyOrder: buyList) {
    			count += buyOrder.getAmount();
    		}
    		
    		return count;
    	} else {
    		return 0;
    	}
    }
    
    /**
     * Get number of existed stocks
     * 
     * @return Number of existed stocks
     */
    public int getExistedCount() {
    	return getBuyCount() - getSellCount();
    }
    
    public double getSellPrice() {
    	if (sellList != null) {
    		double price = 0;
    		
    		for (final StockSell sellOrder: sellList) {
    			price += sellOrder.getPrice() * sellOrder.getAmount();
    		}
    		
    		return price;
    	} else {
    		return 0;
    	}
    }
    
    public double getBuyPrice() {
    	if (buyList != null) {
    		double price = 0;
    		
    		for (final StockBuy buyOrder: buyList) {
    			price += buyOrder.getPrice() * buyOrder.getAmount();
    		}
    		
    		return price;
    	} else {
    		return 0;
    	}
    }
    
    public double getProfit() {
    	return getSellPrice() - getBuyPrice();
    }
}
