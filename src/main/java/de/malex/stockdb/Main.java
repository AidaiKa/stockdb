package de.malex.stockdb;
	
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

import de.malex.stockdb.controllers.MainViewController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;

/**
 * Main class
 * 
 * @author Alexandr Mitiaev
 */
public class Main extends Application {
	/**
	 * Application version
	 */
	public static final String APP_VERSION				=				"0.9.1";
	
	/**
	 * Bundle directory path
	 */
	public static final String BUNDLE_DIR				=				"de/malex/stockdb/i18n/strings";
	
	/**
	 * Path to the form CSS file
	 */
	public static final String CSS_FILE					=				"views/application.css";
	
	/**
	 * Path to the main form .fxml file
	 */
	public static final String VIEW_MAIN				=				"views/MainView.fxml";
	
	/**
	 * The primary stage
	 */
	private static Stage primaryStage;
	
	/**
	 * The {@link MainViewController}
	 */
	private static MainViewController mainController;
	
	/**
	 * The {@link ResourceBundle} for string localization
	 */
	private static ResourceBundle bundle;
	
	/**
	 * The {@link Preferences} to store common app settings
	 */
	private static Preferences prefs;
	
	/**
	 * List of the {@link AppEventListener}
	 */
	private static ArrayList<AppEventListener> listeners = new ArrayList<AppEventListener>();
	
	/**
	 * Return the primary {@link Stage}
	 * 
	 * @return Primary {@link Stage}
	 */
	public static Stage getPrimaryStage() {
		return primaryStage;
	}
	
	/**
	 * Return the {@link MainViewController}
	 * 
	 * @return The {@link MainViewController}
	 */
	public static MainViewController getMainController() {
		return mainController;
	}
	
	/**
	 * Returns string with localization, if need
	 * 
	 * @param str String to localization
	 * 
	 * @return Localized string
	 */
	public static String getLocalizedString(String str) {
		if (str != null && str.charAt(0) == '%') {
			return bundle.getString(str.substring(1));
		} else {
			return str;
		}
	}
	
	/**
	 * Returns the {@link ResourceBundle}
	 */
	public static ResourceBundle getResourceBundle() {
		return bundle;
	}
	
	/**
	 * Returns the {@link #prefs}
	 * 
	 * @return The {@link #prefs}
	 */
	public static Preferences getPrefs() {
		return prefs;
	}
	
	/**
	 * Start the primary stage
	 */
	@SuppressWarnings("static-access")
	@Override
	public void start(Stage primaryStage) {
		try {
			prefs = Preferences.userNodeForPackage(getClass());
			String lang = prefs.get(Constants.PREFS_LOCALE, Constants.LOCALE_EN);
			
			bundle = ResourceBundle.getBundle(BUNDLE_DIR, new Locale(lang, lang.toUpperCase()));
			
			FXMLLoader loader = new FXMLLoader(getClass().getResource(VIEW_MAIN));
			loader.setResources(bundle);
			
            BorderPane root = loader.load();
            
            mainController = loader.getController();
			
			Scene scene = new Scene(root);
			
			this.primaryStage = primaryStage;
			this.primaryStage.setTitle(String.format("%s, v. %s", getLocalizedString("%appTitle"), APP_VERSION));
			
			this.primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
				public void handle(WindowEvent event) {
					for (AppEventListener listener: listeners) {
						listener.onAppClose();
					}
				}
			});
			
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			Main.showAlert(AlertType.ERROR, "Error", "App launch error", e.getMessage());
		}
	}
	
	/**
	 * Add {@link AppEventListener} to listen application events
	 * 
	 * @param listener The instance of the {@link AppEventListener}
	 */
	public static void addListener(AppEventListener listener) {
		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}
	
	/**
	 * Remove the {@link AppEventListener}
	 * 
	 * @param listener The {@link AppEventListener} to remove
	 */
	public static void removeListener(AppEventListener listener) {
		if (listeners.contains(listener)) {
			listeners.remove(listener);
		}
	}
	
	/**
	 * Select folder and return path as a string
	 * 
	 * @return Selected folder or null
	 */
	public static String selectFolder(final String title, final File initialDir) {
		DirectoryChooser chooser = new DirectoryChooser();
		chooser.setTitle(title);
		
		if (initialDir != null && initialDir.exists()) {
			chooser.setInitialDirectory(initialDir);
		}

		File selectedDirectory = chooser.showDialog(primaryStage);
		
		if (selectedDirectory == null || !selectedDirectory.exists()) {
			return null;
		} else {
			return selectedDirectory.getAbsolutePath();
		}
	}
	
	/**
	 * Show open file dialog
	 * 
	 * @param title The open dialog window title
	 * 
	 * @return Selected file or null, if canceled
	 */
	public static File openFileDialog(String title, File initialDir, Collection<ExtensionFilter> filters) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle(title);
		
		if (initialDir != null) {
			fileChooser.setInitialDirectory(initialDir);
		}
		
		if (filters != null) {
			fileChooser.getExtensionFilters().addAll(filters);
		}
		
		return fileChooser.showOpenDialog(primaryStage);
	}
	
	/**
	 * Show confirmation dialog
	 * 
	 * @return True if YES pressed, otherwise false
	 */
	public static boolean confirmDialog(final String title, final String text) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		
		alert.setTitle(title);
		alert.setHeaderText("Confirmation");
		alert.setContentText(text);

		Optional<ButtonType> result = alert.showAndWait();
		
		if (result.get() == ButtonType.OK){
		    return true;
		} else {
		    return false;
		}
	}
	
	/**
	 * Display alert dialog with a message
	 * 
	 * @param alertType
	 *            Type of alert (Error, Information, Confirmation, Warning)
	 * @param title
	 *            Title of the dialog
	 * @param header
	 *            Header of the dialog
	 * @param content
	 *            Message text
	 * @param guiThread
	 * 			  Create a JavaFX-thread
	 */
	public static void showAlert(final AlertType alertType, final String title, final String header, final String content) {
		final String locTitle = getLocalizedString(title);
		final String locHeader = getLocalizedString(header);
		final String locContent = getLocalizedString(content);
		
		if (!Platform.isFxApplicationThread()) {
			Platform.runLater(new Runnable() {
				public void run() {
					Alert alert = new Alert(alertType);

					alert.setTitle(locTitle);
					alert.setHeaderText(locHeader);
					alert.setContentText(locContent);
					alert.show();
				}
			});
		} else {
			Alert alert = new Alert(alertType);

			alert.setTitle(locTitle);
			alert.setHeaderText(locHeader);
			alert.setContentText(locContent);
			alert.show();			
		}
	}
	
	/**
	 * Starting app
	 * 
	 * @param args Command line arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
