package de.malex.stockdb.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * The controller for the ProgressView.fxml form
 * 
 * @author Alexandr Mitiaev
 */
public class BuySellViewController implements Initializable {
	/**
	 * Window stage
	 */
	private Stage stage;
	
	/**
	 * Actuall edited stock
	 */
	@FXML
	private ComboBox<String> cbStock;
	
	/**
	 * Stock number
	 */
	@FXML
	private TextField edNumber;
	
	/**
	 * Stock price
	 */
	@FXML
	private TextField edPrice;
	
	/**
	 * Initialize form
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}
	
	/**
	 * Set up views stage
	 * 
	 * @param stage The {@link Stage}
	 */
	public void init(Stage stage) {
		this.stage = stage;
	}

	/**
	 * Save button clicked
	 */
	@FXML
	public void onCloseClicked() {
		stage.close();
	}
}
