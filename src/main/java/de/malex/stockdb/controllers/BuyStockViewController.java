package de.malex.stockdb.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

/**
 * The controller for the ProgressView.fxml form
 * 
 * @author Alexandr Mitiaev
 */
public class BuyStockViewController implements Initializable {
	/**
	 * "Cancel" button was pressed after start
	 */
	private boolean isCanceled;

	/**
	 * Window stage
	 */
	private Stage stage;

	/**
	 * Initialize form
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}
	
	/**
	 * Set up views stage
	 * 
	 * @param stage The {@link Stage}
	 */
	public void init(Stage stage) {
		this.stage = stage;
	}

	/**
	 * Cancel button clicked
	 */
	@FXML
	public void onCancelClicked() {
		this.isCanceled = true;
		stage.close();
	}
	
	/**
	 * Save button clicked
	 */
	@FXML
	public void onSaveClicked() {

		this.isCanceled = false;
		
		stage.close();
	}
	
	/**
	 * Return true, if dialog was canceled, otherwise false
	 * 
	 * @return True, if dialog was canceled, otherwise false
	 */
	public boolean isCanceled() {
		return isCanceled;
	}
}
