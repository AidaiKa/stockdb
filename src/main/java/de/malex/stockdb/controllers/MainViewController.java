package de.malex.stockdb.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import de.malex.stockdb.AppEventListener;
import de.malex.stockdb.Constants;
import de.malex.stockdb.Main;
import de.malex.stockdb.TimeoutController;
import de.malex.stockdb.TimeoutController.TimeoutException;
import de.malex.stockdb.db.DBFacade;
import de.malex.stockdb.db.model.Stock;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.Scene;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * The controller for MainView.fxml form
 * 
 * @author Alexandr
 */
public class MainViewController implements Initializable, AppEventListener {
	/**
	 * Initial directory
	 */
	public static final String INITIAL_DIR				=				"/";
	
	/**
	 * The {@link Stage} of the ProgressView.fxml form
	 */
	private Stage buySellStage = null;
	
	/**
	 * The controller of the ProgressView.fxml form
	 */
	private BuySellViewController buySellController = null;

	/**
	 * The about view stage
	 */
	private Stage aboutStage = null;
	
	/**
	 * The {@link SettingsViewController} to use
	 */
	private AboutViewController aboutController = null;

	/**
	 * "English" menu item
	 */
	@FXML
	private CheckMenuItem mnuLangEn;
	
	/**
	 * "Deutsch" menu item
	 */
	@FXML
	private CheckMenuItem mnuLangDe;
	
	/**
	 * List of users
	 */
	@FXML
	private TableView<Stock> tblStocks;
	
	/**
	 * List of users
	 */
	private ObservableList<Stock> stocks;
	
	/**
	 * "Sell order" button
	 */
	@FXML
	private Button btnSell;
	
	/**
	 * Event handler for language selection menu items
	 */
	private EventHandler<ActionEvent> langMenuEventHandler = new EventHandler<ActionEvent>() {
		public void handle(final ActionEvent event) {
			mnuLangEn.setSelected(false);
			mnuLangDe.setSelected(false);
			
			if (event.getSource() == mnuLangEn) {
	        	Main.getPrefs().put(Constants.PREFS_LOCALE, Constants.LOCALE_EN);
	        	mnuLangEn.setSelected(true);
	        } else if (event.getSource() == mnuLangDe) {
	        	Main.getPrefs().put(Constants.PREFS_LOCALE, Constants.LOCALE_DE);
	        	mnuLangDe.setSelected(true);
	        }
	        
	        Main.showAlert(AlertType.INFORMATION, "Language", "The application language has been changed", "Changes will take effect the next time you start the application");
		}
	};

	/**
	 * Initialize form
	 */
	public void initialize(final URL location, final ResourceBundle resources) {
		try {
			Main.addListener(this);

			initMenuItems();
			
			initBuySellDialog();
			initAboutDialog();
			
			DBFacade.openSession();
			
			initStockTable();

		} catch (Exception e) {
			Main.showAlert(AlertType.ERROR, "Error", "Error form initialization", e.getMessage());
		}
	}
	
	/**
	 * App closing: close database
	 */
	@Override
	public void onAppClose() {
		try {
			TimeoutController.execute(new Runnable() {
				@Override
				public void run() {
					DBFacade.closeSession();
				}
			}, 5 * 1000);
		} catch (TimeoutException e) {
		}
	}
	
	/**
	 * Initialize ProgressView.fxml view
	 * 
	 * @throws IOException 
	 */
	private void initBuySellDialog() throws IOException {
		if (buySellStage == null) {
			final FXMLLoader loader = new FXMLLoader(Main.class.getResource("views/BuySellView.fxml"));
			loader.setResources(Main.getResourceBundle());
		
			buySellStage = new Stage();
			
			final AnchorPane pane = (AnchorPane)loader.load();
			buySellController = loader.getController();
			
			buySellStage.setScene(new Scene(pane));
			buySellStage.setTitle("Buy/sell stock");
			buySellStage.setResizable(false);
			buySellStage.setAlwaysOnTop(true);
			
			buySellController.init(buySellStage);
		}
	}
	
	/**
	 * Initialize AboutView.fxml view
	 * 
	 * @throws IOException 
	 */
	private void initAboutDialog() throws IOException {
		if (aboutStage == null) {
			final FXMLLoader loader = new FXMLLoader(Main.class.getResource("views/AboutView.fxml"));
			loader.setResources(Main.getResourceBundle());
		
			aboutStage = new Stage();
			
			final AnchorPane pane = (AnchorPane)loader.load();
			aboutController = loader.getController();
			
			aboutStage.setScene(new Scene(pane));
			aboutStage.setTitle("About...");
			aboutStage.setResizable(false);
			aboutStage.setAlwaysOnTop(true);
			
			aboutController.init(aboutStage);
		}
	}

	/**
	 * Initialize menu items
	 */
	private void initMenuItems() {
		mnuLangEn.setOnAction(langMenuEventHandler);
		mnuLangDe.setOnAction(langMenuEventHandler);
	}
	
	/**
	 * Initialize import data table
	 * @throws Exception 
	 * @throws ClassNotFoundException 
	 */
	@SuppressWarnings("unchecked")
	private void initStockTable() throws ClassNotFoundException, Exception {
		stocks = FXCollections.observableArrayList();
		stocks.addAll(DBFacade.getAllStocks());
		
		TableColumn<Stock, String> colName = new TableColumn<Stock, String>("Aktien");
		colName.setCellValueFactory(new PropertyValueFactory<Stock, String>("name"));
		colName.setPrefWidth(140);
		
		TableColumn<Stock, String> colSymbol = new TableColumn<Stock, String>("Zeichen");
		colSymbol.setCellValueFactory(new PropertyValueFactory<Stock, String>("symbol"));
		colSymbol.setPrefWidth(60);
		
		TableColumn<Stock, Integer> colAmount = new TableColumn<Stock, Integer>("Anzahl");
		colAmount.setCellValueFactory(new PropertyValueFactory<Stock, Integer>("existedCount"));
		colAmount.setPrefWidth(60);
		
		TableColumn<Stock, Double> colProfit = new TableColumn<Stock, Double>("Gewinn");
		colProfit.setCellValueFactory(new PropertyValueFactory<Stock, Double>("profit"));
		colProfit.setPrefWidth(80);

		tblStocks.getColumns().clear();
		tblStocks.getColumns().addAll(colName, colSymbol, colAmount, colProfit);
		tblStocks.setItems(stocks);
		
		tblStocks.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			btnSell.setDisable(newSelection == null);
		});
	}
	
	/**
	 * Open about window
	 */
	@FXML
	public void onAboutClicked() {
		aboutStage.showAndWait();
	}
	
	/**
	 * Show sell window
	 */
	@FXML
	public void onSellClicked() {
		buySellStage.setTitle("Aktienverkauf");
		buySellStage.showAndWait();
	}
	
	/**
	 * Show buy window
	 */
	@FXML
	public void onBuyClicked() {
		buySellStage.setTitle("Aktienkauf");
		buySellStage.showAndWait();
	}
}
