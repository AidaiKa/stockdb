package de.malex.stockdb.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import de.malex.stockdb.db.model.Stock;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * The controller for the ProgressView.fxml form
 * 
 * @author Alexandr Mitiaev
 */
public class EditStocksViewController implements Initializable {
	/**
	 * Window stage
	 */
	private Stage stage;
	
	/**
	 * Actuall edited stock
	 */
	private Stock editStock = null;
	
	/**
	 * The {@link TableView} to display stock list
	 */
	@FXML
	private TableView<Stock> tblStocks;
	
	/**
	 * Selected stock symbol
	 */
	@FXML
	private TextField edSymbol;
	
	/**
	 * Selected stock name
	 */
	@FXML
	private TextField edName;

	/**
	 * Initialize form
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}
	
	/**
	 * Set up views stage
	 * 
	 * @param stage The {@link Stage}
	 */
	public void init(Stage stage) {
		this.stage = stage;
		initTableView();
	}
	
	/**
	 * Initialize {@link TableView} to display 
	 */
	private void initTableView() {
		TableColumn<Stock, String> colSymbol = new TableColumn<Stock, String>("Symbol");
		colSymbol.setCellValueFactory(new PropertyValueFactory<Stock, String>("symbol"));
		colSymbol.setPrefWidth(140);
		
		TableColumn<Stock, String> colName = new TableColumn<Stock, String>("Name");
		colName.setCellValueFactory(new PropertyValueFactory<Stock, String>("name"));
		colName.setPrefWidth(60);
	}
	
	/**
	 * Save button clicked
	 */
	@FXML
	public void onCloseClicked() {
		stage.close();
	}
}
