# StockDB

## Tables

### Stock

| Field | Type | Description |
|-------|------|-------------|
| id    | Long | Stock ID    |
| name  | String(256) | Stock name |
| symbol | String(16) | Stock symbol |

### StockBuy

### StockSell